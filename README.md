### Java CI/CD project for Automatization course

This is a project for Automatization lab04.

Includes: 
1. Code analysis with SAST
2. Unit tests
3. Jar by mvn package
4. Artifacts by SAST
